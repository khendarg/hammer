#!/usr/bin/env python

import os
import re
import sys
import shutil
import argparse
import tempfile
import subprocess
from Bio import SeqIO
import anvio.clustering
from kevtools_common.types import TCID

def info(*things): print('[INFO]', *things, file=sys.stderr)

def ssearch36(infile, outfile, metric='bits', args=None):
    if args is None: args = []
    cmd = ['ssearch36']
    cmd.extend(['-m', '8'])
    cmd.extend(args)
    cmd.append(infile)
    cmd.append(infile)

    out = subprocess.check_output(cmd).decode('utf-8')
    with open(outfile, 'w') as fh: fh.write(out)

def blastp(infile, outfile, args=None):
    if args is None: args = []
    cmd = ['blastp']
    cmd.extend(['-outfmt', '6'])
    cmd.extend(args)
    cmd.extend(['-query', infile])
    cmd.extend(['-subject', infile])

    out = subprocess.check_output(cmd).decode('utf-8')
    with open(outfile, 'w') as fh: fh.write(out)

def diamond(infile, outfile, args=None):
    tempdir = tempfile.mkdtemp()
    try:
        subprocess.call(['diamond', 'makedb', '-d', '{}/tmpdb'.format(tempdir), '--in', infile])
        cmd = ['diamond']
        cmd.append('blastp')
        cmd.extend(['-d', '{}/tmpdb'.format(tempdir)])
        cmd.extend(['-q', infile])
        cmd.extend(['-f', '6'])
        cmd.extend(['-e', '1'])

        out = subprocess.check_output(cmd).decode('utf-8')
        with open(outfile, 'w') as fh: fh.write(out)

    finally: shutil.rmtree(tempdir)

def parse_blast(infh, metric='bits'):
    distances = {}
    for line in infh:
        if line.startswith('#'): continue
        elif not line.strip(): continue
        sl = line.replace('\n', '').split('\t')
        
        query, subject = sl[0], sl[1]
        if metric == 'bits': distance = float(sl[11])
        else: raise NotImplementedError('Unknown metric: `{}`'.format(metric))
        existing = distances.get(query, {}).get(subject, None)
        existingflip = distances.get(subject, {}).get(query, None)
        if existing is not None:
            distances[query][subject] = max(distances[query][subject], distance)
        elif existingflip is not None:
            distances[subject][query] = max(distances[subject][query], distance)
        else:
            if query not in distances: distances[query] = {}
            distances[query][subject] = distance

    for query in distances:
        for subject in distances[query]:
            if subject not in distances: distances[subject] = {}
            #distances[query][subject] = 2 ** -distances[query][subject]
            try: distances[query][subject] = distances[query][subject] ** -0.25
            except ZeroDivisionError: distances[query][subject] = 1.0
            distances[subject][query] = distances[query][subject]
    return distances

class Famxplorer(object):
    seednames = None
    sequences = None
    def __init__(self, **kwargs):
        self.seednames = kwargs.get('seednames', set())
        self.sequences = kwargs.get('sequences', list())
        self.outdir = kwargs.get('outdir', 'fxplorer_out')

        self.aligner = kwargs.get('aligner', 'ssearch36')
        self.metric = kwargs.get('metric', 'bits')

        self.miscdata = {}
    def main(self, args):
        self.outdir = args.outdir
        if not os.path.isdir(self.outdir): os.mkdir(self.outdir)

        if os.path.isfile('{}/seednames.faa'.format(self.outdir)) and not args.force:
            with open('{}/seednames.faa'.format(self.outdir)) as fh:
                for l in fh: self.seednames.add(l.replace('\n', ''))
        elif args.initial_seq is not None: 
            self.sequences.extend(SeqIO.parse(args.initial_seq, 'fasta'))
            for record in self.sequences: self.seednames.add(record.id)
            with open('{}/seednames.faa'.format(self.outdir), 'w') as fh: 
                fh.write('\n'.join(sorted(self.seednames)))

        if os.path.isfile('{}/sequences.faa'.format(self.outdir)) and not args.force:
            with open('{}/sequences.faa'.format(self.outdir)) as fh: 
                self.sequences.extend(SeqIO.parse(fh, 'fasta'))
        else:
            self.sequences.extend(SeqIO.parse(args.fxpfasta, 'fasta'))
            with open('{}/sequences.faa'.format(self.outdir), 'w') as fh: 
                SeqIO.write(self.sequences, fh, 'fasta')

        self.miscdata['contig'] = {}
        for i, record in enumerate(self.sequences): self.miscdata['contig'][i] = record.id

        self.gen_seqtree(args)

        self.hmmtop_counttms(args)
        self.auto_tcid(args)

        self.export_misc_data(args)

    def gen_seqtree(self, args):
        if os.path.isfile('{}/bittree.nwk'.format(self.outdir)) and not args.force:
            pass
        else:
            if os.path.isfile('{}/bitmatrix.tsv'.format(self.outdir)) and not args.force:
                pass
            else:
                self.aligner = args.aligner
                self.metric = args.distance_metric
                bitdistances = self.run_aligner('{}/sequences.faa'.format(self.outdir), force=args.force)
                bitmatrix = self.write_matrix(bitdistances, open('{}/bitmatrix.tsv'.format(self.outdir), 'w'))
            anvio.clustering.create_newick_file_from_matrix_file('{}/bitmatrix.tsv'.format(self.outdir), '{}/bitmatrix.nwk'.format(self.outdir))

    def hmmtop_counttms(self, args):
        if os.path.isfile('{}/tmpredictions.hmmtop'.format(self.outdir)) and not args.force:
            pass
        else:
            cmd = ['hmmtop', '-if={}/sequences.faa'.format(self.outdir), '-of={}/tmpredictions.hmmtop'.format(self.outdir), '-is=pseudo', '-sf=FAS', '-pi=spred']
            subprocess.call(cmd)

        def _skipspaces(line):
            n = 0
            for i, c in enumerate(line):
                if c == ' ': n += 1
                if n >= 2: return line[i+1:]

        with open('{}/tmpredictions.hmmtop'.format(args.outdir)) as fh:
            self.miscdata['TMS_count'] = {}
            for line in fh:
                topout = re.findall(' +(?:IN|OUT)(?: *[0-9]+ *)+', line)
                if not topout: continue
                topout = topout[0]
                namestart = _skipspaces(line)[:-len(topout)].strip()
                topout = topout.strip()
                ntms = (len(topout.strip().split())-2)//2

                self.miscdata['TMS_count'][namestart] = ntms

    def auto_tcid(self, args):
        self.miscdata['TCID'] = {}
        self.miscdata['TC-family'] = {}
        for seqi in self.miscdata['contig']:
            try: tcid = TCID(self.miscdata['contig'][seqi])
            except AssertionError: tcid = None
            self.miscdata['TCID'][self.miscdata['contig'][seqi]] = tcid
            if tcid is None: self.miscdata['TC-family'][self.miscdata['contig'][seqi]] = None
            else: self.miscdata['TC-family'][self.miscdata['contig'][seqi]] = tcid[:3]

    def export_misc_data(self, args):
        with open('{}/miscdata.tsv'.format(self.outdir), 'w') as fh:
            out = ''
            out += 'contig'
            for key in self.miscdata:
                if key != 'contig': out += '\t{}'.format(key)
            out += '\n'

            for seqi in sorted(self.miscdata['contig']):
                seqname = self.miscdata['contig'][seqi]
                out += seqname
                for key in self.miscdata:
                    if key != 'contig': out += '\t{}'.format(self.miscdata[key].get(seqname))
                out += '\n'
            fh.write(out)
        

    def run_aligner(self, infile=None, outfile=None, aligner=None, metric='bits', force=False):
        if infile is None: infile = '{}/sequences.faa'.format(self.outdir)
        if outfile is None: outfile = '{}/alignments.dat'.format(self.outdir)

        if aligner is None: aligner = self.aligner
        info('Performing alignments with {}'.format(aligner))

        if os.path.isfile(outfile) and not force:
            pass
        else:
            if aligner == 'ssearch36': ssearch36(infile, outfile, metric)
            elif aligner == 'blastp': blastp(infile, outfile, metric)
            elif aligner == 'diamond': diamond(infile, outfile, metric)
            else: raise NotImplementedError('Alignment parser for `{}` not implemented'.format(aligner))
        if ('blast' in aligner) or ('ssearch' in aligner) or (aligner in ('diamond', 'mmseqs')):
            return parse_blast(open(outfile), metric=metric)

    def write_matrix(self, distances, outfh):
        labels = set()
        for query in distances:
            labels.add(query)
            for subject in distances:
                labels.add(subject)
        labels = sorted(labels)
        out = ''
        for label in labels:
            out += '\t{}'.format(label)
        out += '\n'
        for labelr in labels:
            out += labelr
            for labelc in labels:
                out += '\t{}'.format(distances.get(labelr, {}).get(labelc, 1.0))
            out += '\n'
        outfh.write(out)
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('fxpfasta', type=argparse.FileType('r'), help='Library of homologs in FASTA format, e.g. (psi)blast/hhblits/mmseqs results')

    parser.add_argument('-i', '--initial-seq', type=argparse.FileType('r'), help='Library of seed sequences (optional)')

    parser.add_argument('-o', '--outdir', required=True, help='Where to save results')

    parser.add_argument('--distance-metric', default='bits', help='Distance metric (default: bits)')
    parser.add_argument('--aligner', default='ssearch36', help='Alignment tool (default: ssearch36)')
    parser.add_argument('-f', '--force', action='store_true', help='Clobber existing files')

    args = parser.parse_args()

    fxpl = Famxplorer()
    fxpl.main(args)
